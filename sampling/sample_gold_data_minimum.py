"""
Sample gold data for hand-labeling.
Make sure to downsample the overrepresented classes,
i.e. #IStandWithAhmed AND minimum sample each class.
"""
import argparse
import pandas as pd
import re, os, codecs

def sample_data(data_files, 
                all_tags,
                restricted_tags, 
                out_dir,
                sample_size=500, 
                max_tag_cap=100,
                min_tag_cap=20):
    """
    Sample data for sample_size tweets, 
    including at least min_tag_cap 
    tweets from each tag and a max of max_tag_cap 
    for restricted tags.

    Parameters:
    -----------
    data_files : [str]
    all_tags : [str]
    restricted_tags : [str]
    out_dir : str
    sample_size : int
    max_tag_cap : int
    min_tag_cap : int

    Returns:
    --------
    sample = pandas.DataFrame
    """
    # make sure that tags fit in sample
    sample_size = max(sample_size, min_tag_cap*len(all_tags))
    # we assume that tweet files have this format -___-
    # fields = 'username;date;retweets;favorites;text;geo;mentions;hashtags;id;permalink;'.split(';')
    all_data = []
    # preprocessing: throw out lines with more than expected number
    # of fields b/c of colons in text
    for tag, data_file in zip(all_tags, data_files):
        # tag_data = {f : [] for f in fields}
        # tag_data['supportTag'] = []
        tag_data = pd.read_csv(data_file, sep=';', 
                               encoding='utf-8', index_col='id',
                               error_bad_lines=False, 
                               warn_bad_lines=False)
        tag_data['supportTag'] = tag
        # with codecs.open(data_file, 'r', encoding='utf-8') as data_in:
#             for i, line in enumerate(data_in):
#                 if(i > 0):
#                     vals = line.split(';')
#                     #if(len(vals) == len(fields)):
#                     for f, val in zip(fields, vals):
#                         tag_data[f].append(val)
#                     tag_data['supportTag'].append(tag)
#         tag_data = pd.DataFrame(tag_data)
        # print('for tag %s extracted tag data of size %s'%
#               (tag, str(tag_data.shape)))
        all_data.append(tag_data)
    all_data = pd.concat(all_data, axis=0)
    # all_data.index = range(all_data.shape[0])
    print('extracted all data of shape %s'%
          (str(all_data.shape)))
    # print('got all data shape %s = %s'%
#           (str(all_data.shape), all_data))
    # get minimum sample from each tag
    sampled_data = []
    for tag, tag_data in all_data.groupby('supportTag'):
        tag_sample_size = min(len(tag_data), min_tag_cap)
        sample_tag_data_indices = pd.np.random.choice(tag_data.index,
                                                      tag_sample_size,
                                                      replace=False)
        # print('tag %s got sample indices %s'%
#               (tag, sample_tag_data_indices))
        sample_tag_data = tag_data.loc[sample_tag_data_indices]
        sampled_data.append(sample_tag_data)
    # now fill in the gaps
    sampled_data = pd.concat(sampled_data, axis=0)
    # print('got sampled data shape %s'%(str(sampled_data.shape)))
    remaining_data = all_data.drop(sampled_data.index,
                                   axis=0, inplace=False)
    # print('remaining data shape %s'%(str(remaining_data.shape)))
    remaining_sample_size = sample_size - sampled_data.shape[0]
    remaining_sample_indices = pd.np.random.choice(remaining_data.index,
                                                   remaining_sample_size,
                                                   replace=False)
    remaining_sample = remaining_data.loc[remaining_sample_indices]
    # print('got remaining sample %s'%(str(remaining_sample.shape)))
    combined_sample = pd.concat([sampled_data, remaining_sample], axis=0)
    # print('got combined sample %s'%(str(combined_sample.shape)))
    # get rid of excess restricted tags and resample if necessary
    restricted_tag_total = combined_sample['supportTag'].value_counts()[restricted_tags].sum()
    print('got pre-remove value counts %s'%(combined_sample['supportTag'].value_counts()))
    print('got %d restricted tags over max cap %d'%
          (restricted_tag_total, max_tag_cap))
    if(restricted_tag_total > max_tag_cap):
        combined_sample_restricted = combined_sample[combined_sample['supportTag'].isin(restricted_tags)]
        replace_count = restricted_tag_total - max_tag_cap
        restricted_to_remove = pd.np.random.choice(combined_sample_restricted.index,
                                                   replace_count,
                                                   replace=False)
        print('going to remove %d restricted tweets'%(replace_count))
        combined_sample.drop(restricted_to_remove, inplace=True, axis=0)
        print('combined sample now has %d tweets'%(combined_sample.shape[0]))
        all_restricted_indices = all_data[all_data['supportTag'].isin(restricted_tags)].index
        all_data_filtered = all_data.drop(all_restricted_indices, 
                                          axis=0, inplace=False)
        # get rest of sample 
        remaining_data = all_data_filtered.drop(combined_sample.index,
                                                axis=0, inplace=False)
        print('got remaining data of shape %s'%(str(remaining_data.shape)))
        # print('remaining data has restricted tweets %s'%(remaining_data[remaining_data['supportTag'].isin(restricted_tags)]))
        remaining_sample_indices = pd.np.random.choice(remaining_data.index,
                                                       replace_count, 
                                                       replace=False)
        remaining_sample = remaining_data.loc[remaining_sample_indices]
        combined_sample = pd.concat([combined_sample, remaining_sample],
                                    axis=0)
    print('got post filter value counts %s'%
          (combined_sample['supportTag'].value_counts()))
    combined_index = combined_sample.index.tolist()
    # print('combined index length %d'%(len(combined_index)))
    pd.np.random.shuffle(combined_index)
    combined_sample = combined_sample.loc[combined_index]
    # print('post-shuffle combined sample shape %s'%(str(combined_sample.shape)))
    combined_sample['id'] = combined_sample.index
    # write to file
    out_file = os.path.join(out_dir, 'sample_gold_data.csv')
    pd.set_option('display.float_format', lambda x: '%d'%(x))
    combined_sample.to_csv(out_file, sep=';', 
                           encoding='utf-8', 
                           index=False,
                           index_label='id',
                           float_format='%d')
    return combined_sample

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('data_files', nargs='+')
    parser.add_argument('--sample_size', default=500)
    parser.add_argument('--min_tag_cap', default=20)
    parser.add_argument('--max_tag_cap', default=100)
    parser.add_argument('--restricted_tags', default=['IStandWithAhmed'], nargs='+')
    parser.add_argument('--out_dir', default='../../data/')
    args = parser.parse_args()
    data_files = args.data_files
    all_tags = [os.path.basename(d).split('_tag')[0] for d in data_files]
    sample_size = args.sample_size
    min_tag_cap = args.min_tag_cap
    max_tag_cap = args.max_tag_cap
    restricted_tags = args.restricted_tags
    out_dir = args.out_dir
    sample_data(data_files, 
                all_tags,
                restricted_tags, 
                out_dir,
                sample_size=sample_size, 
                max_tag_cap=max_tag_cap,
                min_tag_cap=min_tag_cap)

if __name__ == '__main__':
    main()
