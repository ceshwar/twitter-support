"""
Sample gold data for hand-labeling.
Make sure to downsample the overrepresented classes,
i.e. #IStandWithAhmed.
"""
import argparse
from zipfile import ZipFile
import pandas as pd
import re, os

def sample_data(data_file, 
                sample_size, out_dir,
                restricted_tags, restricted_tag_cap):
    """
    Sample data containing at least one of the included tags
    and up to a fixed amount of the restricted tags.
    """
    restricted_tag_ctr = 0
    restricted_tag_matcher = re.compile('|'.join(restricted_tags))
    normal_data = []
    restricted_data = []
    fields = 'username;date;retweets;favorites;text;geo;mentions;hashtags;id;permalink'.split(';')
    with ZipFile(data_file, 'r') as zfile:
        for name in zfile.namelist():
            if(name != 'gold_tweets/'):
                tag = os.path.basename(name).split('.csv')[0].split('_')[0].lower()
                print(tag)
                tag_data = {f : [] for f in fields}
                for i, line in enumerate(zfile.read(name).split('\n')):
                    if(i > 0):
                        vals = line.split(';')
                        for f, val in zip(fields, vals):
                            tag_data[f].append(val)
                tag_data = pd.DataFrame(tag_data)
                if(restricted_tag_matcher.search(tag)):
                    restricted_data.append(tag_data)
                else:
                    normal_data.append(tag_data)
    normal_data = pd.concat(normal_data, axis=0)
    normal_data.index = range(normal_data.shape[0])
    print('got normal data of size %s'%(str(normal_data.shape)))
    restricted_data = pd.concat(restricted_data, axis=0)
    restricted_data.index = range(restricted_data.shape[0])
    print('got restricted data of size %s'%(str(restricted_data.shape)))
    # now sample! normal and restricted data separately pls
    restricted_tag_cap = min(restricted_tag_cap, restricted_data.shape[0])
    normal_data_size = sample_size - restricted_tag_cap
    print('normal data size %s'%(normal_data_size))
    normal_data_indices = pd.np.random.choice(normal_data.index, 
                                              normal_data_size,
                                              replace=False)
    restricted_data_indices = pd.np.random.choice(restricted_data.index, 
                                                  restricted_tag_cap,
                                                  replace=False)
    normal_data = normal_data.loc[normal_data_indices]
    print('normal data now has shape %s'%(str(normal_data.shape)))
    restricted_data = restricted_data.loc[restricted_data_indices]
    print('restricted data now has shape %s'%(str(restricted_data.shape)))
    combined_data = pd.concat([normal_data, restricted_data], axis=0)
    # shuffle data
    combined_index = combined_data.index
    pd.np.random.shuffle(combined_index)
    combined_data = combined_data.loc[combined_index]
    print('combined data has shape %s'%(str(combined_data.shape)))
    # write to file
    out_file = os.path.join(out_dir, 'sample_data.csv')
    combined_data.to_csv(out_file, sep=';')
    return combined_data

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--gold_data', default='../../gold_tweets.zip')
    parser.add_argument('--all_tags', default='clickwithcompassion,istandwithleslie,istandwithnormani,hisnameiscayden,love4gabbyusa,zwagger,istandwithahmed,veterensforkaepernick')
    parser.add_argument('--sample_size', default=500)
    parser.add_argument('--restricted_tags', default='istandwithahmed')
    parser.add_argument('--restricted_tag_cap', default=100)
    parser.add_argument('--out_dir', default='../../data/')
    args = parser.parse_args()
    gold_data = args.gold_data
    all_tags = args.all_tags.split(',')
    sample_size = args.sample_size
    restricted_tags = args.restricted_tags.split(',')
    restricted_tag_cap = args.restricted_tag_cap
    out_dir = args.out_dir
    sample_data(gold_data, sample_size,
                out_dir, 
                restricted_tags, restricted_tag_cap)

if __name__ == '__main__':
    main()
