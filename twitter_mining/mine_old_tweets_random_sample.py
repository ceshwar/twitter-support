"""
Mine tweets from zipped files 
in a random sample, only keeping
the tweets with 
"""
import gzip
import os, re, json, codecs
import random

#URL_REGEX = re.compile(r'^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:/[^\s]*)?$')
URL_REGEX = re.compile(r"http\S+")
MENTION_REGEX = re.compile(r'@\w+')
HASHTAG_REGEX = re.compile(r'#\w+')
RT_REGEX = re.compile(r'RT :')
def preprocess(text):
    """
    Remove all URLs, mentions and hashtags
    from tweet text.

    parameters:
    -----------
    text = str
    
    returns:
    --------
    clean_text = str
    """
    clean_text = URL_REGEX.sub('', MENTION_REGEX.sub('', HASHTAG_REGEX.sub('', RT_REGEX.sub('', text)))).replace('\n', '').strip()
    return clean_text

if __name__ == '__main__':
    # 2016 data
    tweets_dir = '/hg190/corpora/twitter-crawl/new-archive/'
    # 2014-15 data
    # tweets_dir = '/hg190/corpora/twitter-crawl/daily-tweet-archives/'
    tweet_files = [os.path.join(tweets_dir, f) 
                   for f in sorted(os.listdir(tweets_dir))
                   if f[-3:] == '.gz' ]
    out_dir = '../../data/old_tweets/2016_tweets/'
    out_file = os.path.join(out_dir, 'random_sample.txt')
    ctr = 0
    cutoff = 35000
    # make this lower if we want a wider spread of data, 
    # i.e. data from more tweet files
    accept_prob = 0.3
    with codecs.open(out_file, 'w', encoding='utf-8') as output:
        for tweet_file_name in tweet_files:
            print('bout to mine tweet file %s'%(tweet_file_name))
            with gzip.open(tweet_file_name, 'r') as tweet_file:
                for l in tweet_file:
                    try:
                        tweet = json.loads(l)
                        if('delete' not in tweet.keys() and 
                           'retweeted_status' not in tweet.keys() and
                            tweet['lang'] == 'en'):
                            if(accept_prob > random.random()):
                                # print(tweet)
                                # get screenname(s) that match and write tweet to necessary files
                                clean_text = preprocess(tweet['text'])
                                if(clean_text != ''):
                                    # print('done got clean text %s'%(clean_text))
                                    output.write(clean_text + '\n')
                                    ctr += 1
                                    if(ctr % 1000 == 0):
                                        print('processed %d valid tweets'%(ctr))
                                    if(ctr >= cutoff):
                                        break
                    except Exception, e:
                        print('tweet extraction failed because %s'%(e))
                        pass
            if(ctr >= cutoff):
                break
