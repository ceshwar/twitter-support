"""
Testing out how to use
Tweepy for mining statuses
and replies from a specific user.
"""
import tweepy
import json, os, codecs
import datetime as dt
# suppress warnings
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()

def get_status_data(status):
    """
    Extract interesting data from
    status.
    
    parameters:
    status: tweepy.Status
    
    returns:
    data: dict
    """
    status_id = str(status.id)
    status_author = str(status.author.screen_name)
    status_time = str(status.created_at)
    status_text = unicode(status.text).replace('\t', ' ')
    status_favorites = str(status.favorite_count)
    status_retweets = str(status.retweet_count)
    is_retweet = str(status.retweeted)
    data = {
        'id' : status_id,
        'author' : status_author,
        'created_at' : status_time,
        'text' : status_text,
        'favorite_count' : status_favorites,
        'retweet_count' : status_retweets,
        'is_retweet' : is_retweet,
    }
    return data

def get_first_tweet_id(api, date='', days_ago=9, query='a'):
    ''' Function that gets the ID of a tweet. This ID can then be
        used as a 'starting point' from which to search. The query is
        required and has been set to a commonly used word by default.
        The variable 'days_ago' has been initialized to the maximum
        amount we are able to search back in time (9).'''

    if date:
        # return an ID from the start of the given day
        td = date + dt.timedelta(days=1)
        tweet_date = '{0}-{1:0>2}-{2:0>2}'.format(td.year, td.month, td.day)
        tweet = api.search(q=query, count=1, until=tweet_date)
    else:
        # return an ID from __ days ago
        td = dt.datetime.now() - dt.timedelta(days=days_ago)
        tweet_date = '{0}-{1:0>2}-{2:0>2}'.format(td.year, td.month, td.day)
        # get list of up to 10 tweets
        tweet = api.search(q=query, count=10, until=tweet_date)
        print('search limit (start/stop):',tweet[0].created_at)
    # return the id of the first tweet in the list
    return tweet[0].id

def get_write_user_timeline(username, pages, out_file, fieldnames):
    """
    Collect user's timeline and
    write to file.
    
    parameters:
    username: str
    pages: int
    out_file: str
    fieldnames: [str]
    """
    page_range = range(1, pages)
    with codecs.open(out_file, 'w', encoding='utf-8') as output:
        output.write('\t'.join(fieldnames)+'\n')
        for p in page_range:
            timeline = api.user_timeline(screen_name=username, page=p)
            # print('status methods %s'%(dir(timeline[0])))
            for t in timeline:
                data = get_status_data(t)
                fields = [data[f] for f in fieldnames]
                output.write(u'\t'.join(fields)+'\n')
            if(p % 10 == 0):
                print('processed %d pages'%(p))

def get_write_tweets(query, tweet_total, tweets_per_query, 
                     out_file, fieldnames, 
                     until_id=-1, max_days_old=10):
    """
    Get tweets matching search query for
    the specified time period, until 
    total is reached or query runs dry.

    parameters:
    query: str
    tweet_total: int
    tweets_per_query: int
    out_file: str
    fieldnames = [str]
    until_id: int
    max_days_old: int
    """
    tweet_ctr = 0
    since_id = get_first_tweet_id(api, days_ago=(max_days_old-1))
    with codecs.open(out_file, 'w', encoding='utf-8') as output:
        output.write('\t'.join(fieldnames)+'\n')
        while(tweet_ctr < tweet_total):
            # raw_results = tweepy.Cursor(api.search, q=search_query).items(tweet_total)
            # print('got raw results %s'%(raw_results))
            if(until_id <= 0):
                #if(not since_id):
                    #raw_results = api.search(q=search_query, count=tweets_per_query)
                #else:
                # TODO: add try-catch
                raw_results = api.search(q=search_query, count=tweets_per_query,
                                         since_id=str(since_id))
                # print('got initial raw results %s'%(raw_results))
            else:
                #if(not since_id):
                #    raw_results = api.search(q=search_query, count=tweets_per_query,
                #                             until_id=str(until_id-1))
                #else:
                raw_results = api.search(q=search_query, count=tweets_per_query,
                                         max_id=until_id,
                                         since_id=str(since_id))
            # raw_results = api.search(q=search_query, count=tweets_per_query)
            results = [r for r in raw_results]
            if(not raw_results):
                print('finished early')
                break
            for r in results:
                data = get_status_data(r)
                fields = [data[f] for f in fieldnames]
                output.write('\t'.join(fields)+'\n')
            tweet_ctr += len(results)
            if(tweet_ctr % 100 == 0):
                print('collected %d tweets'%(tweet_ctr))
            until_id = results[-1].id - 1
            print('new until id %s'%(str(until_id)))

# TODO: hashtag collection

if __name__ == '__main__':
    creds_file = 'twitter_creds.json'
    with open(creds_file) as creds:
        twitter_creds = json.load(creds)
        consumer_key = twitter_creds['consumer_key']
        consumer_secret = twitter_creds['consumer_secret']
        access_token = twitter_creds['access_token']
        access_secret = twitter_creds['access_secret']
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)
    api = tweepy.API(auth_handler=auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
    # now mine for an example user's timeline
    # username = 'unburntwitch'
    usernames = [
        #'parisjackson', 
        #'jessicaruano', 
        #'emilymaynot', 
        # 'monicalewinsky', 
        # 'heartmobber'
    ]
    out_dir = '../../data/'
    tweet_total = 1000
    tweets_per_query = 100
    until_id = -1
    # 9 days max old
    max_days_old = 9
    fieldnames = ['id', 'author', 'created_at', 'text', 'favorite_count', 'retweet_count', 'is_retweet']
    for username in usernames:
        print('mining user %s'%(username))
        pages = 100
        out_file = os.path.join(out_dir, '%s_timeline.tsv'%(username))
        get_write_user_timeline(username, pages, out_file, fieldnames)
        # next get all user mentions...and later sync them with original tweets? OK
        search_query = '@%s-filter:retweets'%(username)
        out_file = os.path.join(out_dir, '%s_mentions.tsv'%(username))
        get_write_tweets(search_query, tweet_total, tweets_per_query,
                         out_file, fieldnames,
                         until_id=until_id, max_days_old=max_days_old)
    # looking for specific hashtags
    # hashtag = 'antibully'
    # hashtags = ['antibully', 'iammorethan', 'illpeewithyou', 'bullyproof', 'positivity', 'stopbullying', 'whyistayed']
    hashtags = ['antibully']
    for hashtag in hashtags:
        print('mining tag %s'%(hashtag))
        search_query = '#%s'%(hashtag)
        out_file = os.path.join(out_dir, '%s_tag.tsv'%(hashtag))
        get_write_tweets(search_query, tweet_total, tweets_per_query,
                         out_file, fieldnames,
                         until_id=until_id, max_days_old=max_days_old)
