"""
Mine tweets from zipped files 
for a specified hashtag or user.
"""
import gzip
import os, re, json, codecs

if __name__ == '__main__':
    tweets_dir = '/hg190/corpora/twitter-crawl/new-archive/'
    tweet_files = [os.path.join(tweets_dir, f) 
                   for f in sorted(os.listdir(tweets_dir))
                   if f[-3:] == '.gz' ]
    test_file = os.path.join(tweets_dir, 'tweets-Jun-17-16-04-13.gz')
    out_dir = '/hg190/istewart6/support_lexicon/data/'
    # query_term = re.compile('@lesdogg')
    reply_screennames = {'lesdoggg', 'unburntwitch', 
                         'parisjackson', 'jessicaruano', 'emilymaynot', 'monicalewinsky', 'heartmobber'}
    hashtags = {'antibully', 'bullyproof', 'istandwithleslie', 'illridewithyou'}
    out_files = {s : codecs.open(os.path.join(out_dir, '%s_replies.json'%(s)), 
                                 'w', encoding='utf-8')
                 for s in reply_screennames}
    out_files.update({
            h : codecs.open(os.path.join(out_dir, '%s_tags.json'%(h)), 
                            'w', encoding='utf-8')
            for h in hashtags
            })
    for tweet_file_name in tweet_files:
        print('about to mine file %s'%(tweet_file_name))
        with gzip.open(tweet_file_name, 'r') as tweet_file:
            for l in tweet_file:
                try:
                    tweet = json.loads(l)
                    if('delete' not in tweet.keys() and 'retweeted_status' not in tweet.keys()):
                        # print(tweet)
                        # get screenname(s) that match and write tweet to necessary files
                        if(len(tweet['entities']['user_mentions']) > 0):
                            tweet_mentions = {m['screen_name'].lower() for m in tweet['entities']['user_mentions']}
                            overlap_mentions = tweet_mentions & reply_screennames
                            for overlap_mention in overlap_mentions:
                                out_files[overlap_mention].write(json.dumps(tweet)+'\n')
                            # if(len(overlaps) > 0):
                            #     print('successful find %s'%(l))
                            #     break
                        # also get hashtag(s) that match and write tweet
                        if(len(tweet['entities']['hashtags']) > 0):
                            tweet_hashtags = {h['text'].lower() for h in tweet['entities']['hashtags']}
                            overlap_tags = tweet_hashtags & hashtags
                            for overlap_tag in overlap_tags:
                                out_files[overlap_tag].write(json.dumps(tweet)+'\n')
                except Exception, e:
                    # print('tweet extraction failed because %s'%(e))
                    pass
