## compile list of hashtags that co-occur (in tweets) with known support hashtags
from twokenize import tokenizeRawTweetText as tokenize
import os

#list of tweets collected for known support hashtags
filepath = "tags/"
filelist = os.listdir(filepath)

#create a tag dictionary
co_tags = dict()

for file_ in filelist:
    #read file
    if '.tsv' not in file_:
        continue
    print 'File: ', file_
#     df = pd.read_csv(filepath + filelist[0], sep = '\t')
    df = pd.read_csv(filepath + file_, sep = '\t')
    
    #tokenize each line and build vocabulary
    for line in df.text:
    #     print line
        ##tokenize
        try:
            tweet = tokenize(line)
        except:
            continue

        for token in tweet:
            if token[0] == '#':
                print "Hashtag: ", token
                ##update co_tags dict
                if token in co_tags:
                    #update count
                    co_tags[token] = co_tags[token] + 1
                else:
                    #add to dict
                    co_tags[token] = 1

print 'Have co-tags for all (known) support hashtags'

#sort dictionary by value (frequency of (co-)occurrence of each hashtag)
import operator
sorted_x = sorted(co_tags.items(), key=operator.itemgetter(1), reverse = True)
print "top 250:"
sorted_x[:250]
